require_relative 'boot'

require 'rails/all'

require "view_component/engine"
# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
module PemasBaseTemplate
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0
    if ['development', 'test'].include? ENV['RAILS_ENV']
        Dotenv::Railtie.load
    end
    require "#{Rails.root}/app/utils/string"
    config.autoload_paths += Dir[Rails.root.join('app', 'datatables')]
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
  end
end
