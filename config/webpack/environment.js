const { environment } = require('@rails/webpacker')

const datatables = require('./loaders/datatables')
const webpack = require('webpack')
environment.loaders.append('datatables', datatables)
environment.plugins.prepend('Provide',
  new webpack.ProvidePlugin({
    $: 'jquery/src/jquery',
    jQuery: 'jquery/src/jquery'
  })
)

module.exports = environment
