Rails.application.routes.draw do
  get 'summary/index'
  get 'registered_business/index'
  resources :extra_employees
  resources :townships
  devise_for :users, controllers: {
    sessions: 'users/sessions'
             }


  resources :application_requests
  resources :workplaces
  get 'home/index'
  get 'users', to: "users#index"
  get 'users/new' => "users#new", as: :new_user



  get 'user/:id' => "users#show", as: :get_user
  post 'create_user' => "users#create", as: :create_user
  patch 'update_user/:id' => "users#update", as: :update_user
  get 'edit_user/:id' => "users#edit", as: :edit_user
  delete "destroy_user/:id" => "users#destroy", as: :destroy_user

  get "my_application_requests/:id/new_employees" => "application_requests#new_employees", as: :new_employees
  post "grid_data/batch_create_application_requests/:id", to: "application_requests#batch_create", as: "application_request_batch_create"

  get "my_application_requests/:id/grid" => "application_requests#employee_list", as: :employee_list
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  post "grid_data/application_requests/:id", to: "application_requests#data_update", as: "application_requests_data_update"
  delete "grid_data/batch_delete_application_requests/:id", to: "application_requests#batch_delete", as: "application_request_batch_delete"

  post "change_state_application_request/:id", to: "application_requests#change_state", as: "application_request_change_state"
  post "change_state_workplace/:id", to: "workplaces#change_state", as: "workplace_change_state"
  get "public/new_form" => "public_workplaces#new", as: :new_public_form
  post "public/create_form" => "public_workplaces#create", as: :create_public_form
  get "public/success" => "public_workplaces#success", as: :success_form
  root "public_workplaces#index"



  get 'workplaces' => "workplaces#index", aasm_state: "pending", as: "workplaces_index"
  get 'summary/index'
  post 'summary/print'

  post "print_application_request" => "application_requests#print"

end
