require 'rails_helper'

RSpec.describe "extra_employees/new", type: :view do
  before(:each) do
    assign(:extra_employee, ExtraEmployee.new(
      ward: "MyString",
      street: "MyString",
      house_no: "MyString",
      name: "MyString",
      gender: "MyString",
      nric: "MyString",
      job: "MyString",
      workplace: "MyString"
    ))
  end

  it "renders new extra_employee form" do
    render

    assert_select "form[action=?][method=?]", extra_employees_path, "post" do

      assert_select "input[name=?]", "extra_employee[ward]"

      assert_select "input[name=?]", "extra_employee[street]"

      assert_select "input[name=?]", "extra_employee[house_no]"

      assert_select "input[name=?]", "extra_employee[name]"

      assert_select "input[name=?]", "extra_employee[gender]"

      assert_select "input[name=?]", "extra_employee[nric]"

      assert_select "input[name=?]", "extra_employee[job]"

      assert_select "input[name=?]", "extra_employee[workplace]"
    end
  end
end
