require 'rails_helper'

RSpec.describe "extra_employees/index", type: :view do
  before(:each) do
    assign(:extra_employees, [
      ExtraEmployee.create!(
        ward: "Ward",
        street: "Street",
        house_no: "House No",
        name: "Name",
        gender: "Gender",
        nric: "Nric",
        job: "Job",
        workplace: "Workplace"
      ),
      ExtraEmployee.create!(
        ward: "Ward",
        street: "Street",
        house_no: "House No",
        name: "Name",
        gender: "Gender",
        nric: "Nric",
        job: "Job",
        workplace: "Workplace"
      )
    ])
  end

  it "renders a list of extra_employees" do
    render
    assert_select "tr>td", text: "Ward".to_s, count: 2
    assert_select "tr>td", text: "Street".to_s, count: 2
    assert_select "tr>td", text: "House No".to_s, count: 2
    assert_select "tr>td", text: "Name".to_s, count: 2
    assert_select "tr>td", text: "Gender".to_s, count: 2
    assert_select "tr>td", text: "Nric".to_s, count: 2
    assert_select "tr>td", text: "Job".to_s, count: 2
    assert_select "tr>td", text: "Workplace".to_s, count: 2
  end
end
