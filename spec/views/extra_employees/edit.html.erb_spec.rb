require 'rails_helper'

RSpec.describe "extra_employees/edit", type: :view do
  before(:each) do
    @extra_employee = assign(:extra_employee, ExtraEmployee.create!(
      ward: "MyString",
      street: "MyString",
      house_no: "MyString",
      name: "MyString",
      gender: "MyString",
      nric: "MyString",
      job: "MyString",
      workplace: "MyString"
    ))
  end

  it "renders the edit extra_employee form" do
    render

    assert_select "form[action=?][method=?]", extra_employee_path(@extra_employee), "post" do

      assert_select "input[name=?]", "extra_employee[ward]"

      assert_select "input[name=?]", "extra_employee[street]"

      assert_select "input[name=?]", "extra_employee[house_no]"

      assert_select "input[name=?]", "extra_employee[name]"

      assert_select "input[name=?]", "extra_employee[gender]"

      assert_select "input[name=?]", "extra_employee[nric]"

      assert_select "input[name=?]", "extra_employee[job]"

      assert_select "input[name=?]", "extra_employee[workplace]"
    end
  end
end
