require 'rails_helper'

RSpec.describe "extra_employees/show", type: :view do
  before(:each) do
    @extra_employee = assign(:extra_employee, ExtraEmployee.create!(
      ward: "Ward",
      street: "Street",
      house_no: "House No",
      name: "Name",
      gender: "Gender",
      nric: "Nric",
      job: "Job",
      workplace: "Workplace"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Ward/)
    expect(rendered).to match(/Street/)
    expect(rendered).to match(/House No/)
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Gender/)
    expect(rendered).to match(/Nric/)
    expect(rendered).to match(/Job/)
    expect(rendered).to match(/Workplace/)
  end
end
