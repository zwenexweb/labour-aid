require 'rails_helper'

RSpec.describe "workplaces/show", type: :view do
  before(:each) do
    @workplace = assign(:workplace, Workplace.create!(
      name: "Name",
      address: "MyText",
      request_limit: 2,
      created_by: 3
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
  end
end
