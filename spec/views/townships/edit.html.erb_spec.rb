require 'rails_helper'

RSpec.describe "townships/edit", type: :view do
  before(:each) do
    @township = assign(:township, Township.create!(
      ts_pcode: "MyString",
      name: "MyString"
    ))
  end

  it "renders the edit township form" do
    render

    assert_select "form[action=?][method=?]", township_path(@township), "post" do

      assert_select "input[name=?]", "township[ts_pcode]"

      assert_select "input[name=?]", "township[name]"
    end
  end
end
