require 'rails_helper'

RSpec.describe "townships/show", type: :view do
  before(:each) do
    @township = assign(:township, Township.create!(
      ts_pcode: "Ts Pcode",
      name: "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Ts Pcode/)
    expect(rendered).to match(/Name/)
  end
end
