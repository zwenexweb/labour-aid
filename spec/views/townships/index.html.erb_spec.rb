require 'rails_helper'

RSpec.describe "townships/index", type: :view do
  before(:each) do
    assign(:townships, [
      Township.create!(
        ts_pcode: "Ts Pcode",
        name: "Name"
      ),
      Township.create!(
        ts_pcode: "Ts Pcode",
        name: "Name"
      )
    ])
  end

  it "renders a list of townships" do
    render
    assert_select "tr>td", text: "Ts Pcode".to_s, count: 2
    assert_select "tr>td", text: "Name".to_s, count: 2
  end
end
