require 'rails_helper'

RSpec.describe "townships/new", type: :view do
  before(:each) do
    assign(:township, Township.new(
      ts_pcode: "MyString",
      name: "MyString"
    ))
  end

  it "renders new township form" do
    render

    assert_select "form[action=?][method=?]", townships_path, "post" do

      assert_select "input[name=?]", "township[ts_pcode]"

      assert_select "input[name=?]", "township[name]"
    end
  end
end
