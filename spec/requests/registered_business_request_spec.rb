require 'rails_helper'

RSpec.describe "RegisteredBusinesses", type: :request do

  describe "GET /index" do
    it "returns http success" do
      get "/registered_business/index"
      expect(response).to have_http_status(:success)
    end
  end

end
