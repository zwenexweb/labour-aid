require "rails_helper"

RSpec.describe TownshipsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/townships").to route_to("townships#index")
    end

    it "routes to #new" do
      expect(get: "/townships/new").to route_to("townships#new")
    end

    it "routes to #show" do
      expect(get: "/townships/1").to route_to("townships#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/townships/1/edit").to route_to("townships#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/townships").to route_to("townships#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/townships/1").to route_to("townships#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/townships/1").to route_to("townships#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/townships/1").to route_to("townships#destroy", id: "1")
    end
  end
end
