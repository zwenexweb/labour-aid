require "rails_helper"

RSpec.describe ApplicationRequestsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/application_requests").to route_to("application_requests#index")
    end

    it "routes to #new" do
      expect(get: "/application_requests/new").to route_to("application_requests#new")
    end

    it "routes to #show" do
      expect(get: "/application_requests/1").to route_to("application_requests#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/application_requests/1/edit").to route_to("application_requests#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/application_requests").to route_to("application_requests#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/application_requests/1").to route_to("application_requests#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/application_requests/1").to route_to("application_requests#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/application_requests/1").to route_to("application_requests#destroy", id: "1")
    end
  end
end
