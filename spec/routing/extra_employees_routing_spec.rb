require "rails_helper"

RSpec.describe ExtraEmployeesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/extra_employees").to route_to("extra_employees#index")
    end

    it "routes to #new" do
      expect(get: "/extra_employees/new").to route_to("extra_employees#new")
    end

    it "routes to #show" do
      expect(get: "/extra_employees/1").to route_to("extra_employees#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/extra_employees/1/edit").to route_to("extra_employees#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/extra_employees").to route_to("extra_employees#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/extra_employees/1").to route_to("extra_employees#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/extra_employees/1").to route_to("extra_employees#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/extra_employees/1").to route_to("extra_employees#destroy", id: "1")
    end
  end
end
