# coding: utf-8
require 'myanmar-tools'
require 'rabbit'



class NilClass

    def yn_convert
    end
    def male_female
    end
    def myanmar_numbers_to_english
    end
    def english_numbers_to_myanmar
    end

    def force_unicode
    end
end

class Integer
  def myanmar_numbers_to_english
    self.to_s.myanmar_numbers_to_english
  end
  def english_numbers_to_myanmar
    self.to_s.english_numbers_to_myanmar
  end
end
class String

  def force_unicode
    detector = MyanmarTools::ZawgyiDetector.new
    score    = detector.get_zawgyi_probability(self)
    rabbit = Rabbit::Converter.new
    if score > 0.9
      rabbit.zg2uni(self)
    else
       self

    end
  end
  def months_to_mm
    months =
      {
        "Jan": "ဇန်နဝါရီ",
       "Feb": "ဖေဖော်ဝါရီ",
       "Mar": "မတ်",
       "April": "ဧပြီ",
       "May": "မေ",
       "June": "ဇွန်",
       "July": "ဇူလိုင်",
       "Aug": "သြဂုတ်",
       "Sep": "စက်တင်ဘာ",
       "Oct": "အောက်တိုဘာ",
       "Nov": "နိုဝင်ဘာ",
       "Dec": "ဒီဇင်ဘာ"
      }
    months[self]
  end

  def english_numbers_to_myanmar
    if self.nil?
      return
    end
    self.gsub("0","၀")
      .gsub("1","၁")
      .gsub("2","၂")
      .gsub("3","၃")
      .gsub("4","၄")
      .gsub("5","၅")
      .gsub("6","၆")
      .gsub("7","၇")
      .gsub("8","၈")
      .gsub("9","၉")
  end

  def myanmar_numbers_to_english
    if self.nil?
      return
    end
    self.gsub("၀","0")
      .gsub("၁","1")
      .gsub("၂","2")
      .gsub("၃","3")
      .gsub("၄","4")
      .gsub("၅","5")
      .gsub("၆","6")
      .gsub("၇","7")
      .gsub("၈","8")
      .gsub("၉","9")
  end

  def yn_convert
    if self.nil?
      return
    end
    case self.gsub(" ","")
    when "Y"
      "Y"
    when "N"
      "N"
    when "yes"
      "Y"
    when "no"
      "N"
    when "Yes"
      "Y"
    when "No"
      "N"
    when "y"
      "Y"
    when "n"
      "N"
    else
      ""
    end
  end

  def male_female
    if self.nil?
      return
    end
    case self.gsub(" ","")
    when "Male"
      "Male"
    when "Female"
      "Female"
    when "male"
      "Male"
    when "female"
      "Female"
    when "M"
      "Male"
    when "F"
      "Female"
    else
      ""
    end
  end

  def slash_dash
    if self.nil?
      return
    end
    self.gsub("/","-")
  end
end
