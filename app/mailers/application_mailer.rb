class ApplicationMailer < ActionMailer::Base
  default from: 'auto@yangon.gov.mm'
  layout 'mailer'
end
