# coding: utf-8
class SummarySerializer < ActiveModel::Serializer
  attributes :actions,:id, :submit_at, :workplace_name, :owner_name, :zone, :online,:verified, :total, :male, :female, :approved, :status
  include Rails.application.routes.url_helpers
  include ActionView::Helpers::UrlHelper

  def status
    I18n.t(object.aasm_state)
  end

  def actions
    links = ""
    user = User.find(@instance_options[:user_id])
    links += link_to("Details",application_request_path(object),class: "ml-2", target: "_blank")
    if user.admin? or user.sys_admin?

    if !object.rejected? and !object.approved?
      if object.verified?
        links += " | " +  link_to("Approve",application_request_change_state_path(object, state: "approve"), method: :post, class: "text-success ml-2", data: { confirm: "Are you sure to approve" })
        links += " | " +       link_to("Reject",application_request_change_state_path(object, state: "reject"), method: :post, class: "text-danger ml-2", data: { confirm: "Are you sure to reject" })
      end
    end
    end
    links
  end

  def workplace_name
    object.workplace.name
  end

  def submit_at
    object.submit_at&.strftime("%d-%b-%Y") || object.created_at.to_date&.strftime("%d-%b-%Y")
  end
  def owner_name
    object.workplace.owner.name
  end

  def zone
    object.workplace.township.name
  end

  def online
    if object.aasm_state == "draft"
      "မပြီး"
    else
      "ပြီး ✓"
    end
  end

  def verified
    if object.aasm_state == "draft" || object.aasm_state == "submitted"
      "မပြီး"
    else
      "ပြီး ✓"
    end
  end

  def male
    object.employees.where(gender: "Male").or(object.employees.where(gender: "male"))
      .or(object.employees.where(gender: "M"))
      .or(object.employees.where(gender: "ကျား"))
      .or(object.employees.where(gender: "m")).count

  end

  def approved
    if object.aasm_state == "approved"
      "ပြီး ✓ - #{object.approve_at&.strftime("%d-%b-%Y")}"
    else
      "မပြီး"
    end
  end

  def female
    object.employees.where(gender: "Female").or(object.employees.where(gender: "female"))
      .or(object.employees.where(gender: "FeMale"))
      .or(object.employees.where(gender: "F"))
      .or(object.employees.where(gender: "မ"))
      .or(object.employees.where(gender: "f")).count
  end

  def total
    object.employees.count
  end

end
