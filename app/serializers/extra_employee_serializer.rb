class ExtraEmployeeSerializer < ActiveModel::Serializer
  attributes :id, :ward, :street, :house_no, :name, :gender, :nric, :date_of_birth, :job, :workplace
end
