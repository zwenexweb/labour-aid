class EmployeeSerializer < ActiveModel::Serializer
  attributes :id,:name,:gender,:nric,:date_of_birth,:position,:salary,:ssn_employee_no

  def date_of_birth
    if object.date_of_birth.present?
      object.date_of_birth.strftime("%d-%m-%Y")
    end
  end
end
