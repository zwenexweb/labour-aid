class CompaniesMicDatatable < AjaxDatatablesRails::ActiveRecord
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format

    @view_columns ||= CompaniesMic.view_columns
  end

  def data
    records.map do |record|
      record.attributes.map { |k, v| [k, v.to_s] }.to_h

    end

  end

  def user
    @user ||= options[:user]
  end

  def aasm_state
    @aasm_state ||= options[:aasm_state]
  end

  def get_raw_records
    CompaniesMic.all.where(tspcode: user.townships.pluck(:ts_pcode))
    # insert query here
    # User.all
  end

end
