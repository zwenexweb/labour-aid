class WorkplaceDatatable < AjaxDatatablesRails::ActiveRecord

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format

    @view_columns ||= Workplace.view_columns
  end

  def data

    records.map do |record|
      record.attributes.merge(record.custom_attributes)
    end
  end

  def user
    @user ||= options[:user]
  end

  def aasm_state
    @aasm_state ||= options[:aasm_state]
  end

  def get_raw_records
    user.my_workplaces.includes([:owner,:township]).joins([:owner,:township]).where(aasm_state: aasm_state)
    # insert query here
    # User.all
  end

end
