class TownshipDatatable < AjaxDatatablesRails::ActiveRecord

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format

    @view_columns ||= Township.view_columns
  end

  def data

    records.map do |record|
      record.attributes
    end
  end

  def get_raw_records
    Township.all
    # insert query here
    # User.all
  end

end
