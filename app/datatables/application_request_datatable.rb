class ApplicationRequestDatatable < AjaxDatatablesRails::ActiveRecord

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= ApplicationRequest.view_columns
  end

  def data
    records.map do |record|
      record.attributes.merge(record.custom_attributes)
    end
  end

  def user
    @user ||= options[:user]
  end

  def get_raw_records
    # insert query here
    application_requests =  ApplicationRequest.all.joins(:workplace).includes(:workplace)

    application_requests.merge(user.my_workplaces)


  end

end
