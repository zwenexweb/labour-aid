class CompaniesIndustryDatatable < AjaxDatatablesRails::ActiveRecord
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format

    @view_columns ||= CompaniesIndustry.view_columns
  end

  def data
    records.map do |record|
      record.attributes.map { |k, v| [k, v.to_s] }.to_h

    end
  end

  def user
    @user ||= options[:user]
  end


  def get_raw_records
    CompaniesIndustry.all.where(tspcode: user.townships.pluck(:ts_pcode))
    # insert query here
    # User.all
  end

end
