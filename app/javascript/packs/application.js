// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
import { Application } from 'stimulus'
import { definitionsFromContext } from 'stimulus/webpack-helpers'

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")


// Uncomment to copy all static images under ../images to the output folder and reference

// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)

require('jquery')

require('datatables.net-bs4')(window, $)
require('datatables.net-buttons-bs')(window, $)
require('datatables.net-buttons/js/buttons.colVis.js')(window, $)
require('datatables.net-buttons/js/buttons.html5.js')(window, $)
require('datatables.net-buttons/js/buttons.print.js')(window, $)
require('datatables.net-responsive-bs4')(window, $)
require('datatables.net-select')(window, $)


import '../packs/date_cell_editor';

import Swal from 'sweetalert2'
import Rails from "@rails/ujs"
//import debounced from 'debounced'

//debounced.initialize({ input: { wait: 500 } })

const handleConfirm = (e) => {

    Swal.fire({
	title: 'သေချာပါသလား',
	text: "",
	icon: 'warning',
	showCancelButton: true,
	confirmButtonColor: '#347764',
	cancelButtonColor: '#d33',
	confirmButtonText: 'Yes'
    }).then((result) => confirmed(e,result))

    const confirmed = (element, result) => {
	if (result.value) {
	    // User clicked confirm button

	    element.target.removeAttribute('data-sweet-alert-confirm')
	    element.target.click()
	}
    }
    Rails.stopEverything(e);


  // Do your thing
}

// Add event listener before the other Rails event listeners like the one
// for `method: :delete`
Rails.delegate(document, 'a[data-sweet-alert-confirm]', 'click', handleConfirm)





import "controllers"
