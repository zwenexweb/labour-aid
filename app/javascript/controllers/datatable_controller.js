import { Controller } from "stimulus"

export default class extends Controller {
    static targets = [ "table" ]

    connect() {
	console.log("This is connected");
	console.log(this.data.get("keys"));


	var keys = this.data.get("keys");
	var array = [];
	keys.split(",").forEach((x) => {
	    array.push({ "data": x })
	});
	console.log(array);
	$(this.tableTarget).DataTable().destroy();
	window.dataTable = $(this.tableTarget).DataTable({
	    dom: 'Bfrtip',
            buttons: [
		'copy', 'csv', 'excel', 'pdf', 'print'
            ],
	    stateSave: true,
	    "processing": true,
	    "serverSide": true,
	    "ajax": {
		"url": $(this.tableTarget).data('source')
	    },
	    "pagingType": "full_numbers",
	    "columns": array
	    // pagingType is optional, if you want full pagination controls.
	    // Check dataTables documentation to learn more about
	    // available options.
	});
    }

    disconnect(){
	window.dataTable.destroy();
    }


}
