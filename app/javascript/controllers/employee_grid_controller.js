import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

import ApplicationController from './application_controller'
import {Grid} from 'ag-grid-community';
import {LicenseManager} from "ag-grid-enterprise";

import 'ag-grid-enterprise';
import "xlsx";
import Rails from "@rails/ujs";
import DateEditor from '../packs/date_cell_editor';
import Swal from 'sweetalert2';
export default class extends ApplicationController {
    static targets = ["grid"];

    connect() {
    	this.rows = [];
	this.loadGrid();
    }

    loadGrid(){

	LicenseManager.setLicenseKey("CompanyName=Zwenexsys,LicensedGroup=C4M,LicenseType=MultipleApplications,LicensedConcurrentDeveloperCount=1,LicensedProductionInstancesCount=0,AssetReference=AG-010270,ExpiryDate=16_October_2021_[v2]_MTYzNDMzODgwMDAwMA==4bbfea46001dcd4e2023e71d63d7afc7");
	this.grid = new Grid(this.gridTarget,this.gridOptions)
	this.api = this.grid.gridOptions.api
	document.getElementById("rowCount").innerHTML = "(" + this.api.getDisplayedRowCount().toString() + ")"





    }

    saveGrid(e){
	console.log("click saved")
	console.log(this.url);
	Swal.fire({
	    title: 'သိမ်းမည်သေချာပါသလား',
	    text: "",
	    icon: 'warning',
	    showCancelButton: true,
	    confirmButtonColor: '#347764',
	    cancelButtonColor: '#d33',
	    confirmButtonText: 'Yes'
	}).then((result) => confirmed(e,result))
	const confirmed = (element, result) => {
	    if (result.value) {
		// User clicked confirm button
		var data = [];
		this.api.forEachNode( function(rowNode, index) {
                    var tmp = rowNode.data;
                    data.push(tmp);
                });

		fetch(this.url,{
		    method: "POST",
		    headers: {
			"Content-Type": "application/json",
			"Accept": "application/json",
			"X-CSRF-Token": Rails.csrfToken()
		    },
		    body: JSON.stringify({rows: data})
		}).then((response) => {
		    if (response.ok) {
			window.location.href = this.redirectUrl;
		    } else {
			throw new Error("Things went poorly.");
		    }
		})

	    }
	}


    }

    get gridOptions(){
	var rows = window.initialRows;
	console.log(rows);

	return {
	    columnDefs: columnDefs,
	    rowData: rows,
	    browserDatePicker: true,
	    enableColResize: true,
	    enableSorting: true,
	    enableFilter: true,
	    rowSelection: 'multiple',
	    suppressRowClickSelection: true,
	    getRowNodeId: function (data) {
		return data.id;
	    },
	    defaultColDef: {
		filter: true // set filtering on for all columns
	    },
	    components: {
		dateEditor: DateEditor,
	    },
	    onCellValueChanged: (e) => {
		if(this.editable == "true"){
		    if(e.oldValue != e.newValue){
			console.log("Cell value change");
			var id = e.data.id;
			var payload = {}
			let clone = Object.assign(payload, e.data);
			delete payload["delete"];
			delete payload["school"];
			delete payload["inventory_item_category_name"];
			Rails.ajax({
			    url: "/grid_data/application_requests/" + id + ".json",
			    type: "POST",
			    data: "payload=" +JSON.stringify(payload),
			    error: (xhr,error) => {
				alert("Data update failed. Please refresh");
			    },
			    success: (data) => {
				var rowNode = this.api.getRowNode(data.id);
				rowNode.setData(data);
				this.api.flashCells({
				    rowNodes: [rowNode]
				});
			    }
			});
		    }
		}
	    }
	}

    }
    update(e){
	console.log("update event");
	console.log(e.detail.rows);
	this.rows = e.detail.rows
	this.rows.map((i) => {
	    Object.keys(i).forEach((e) => {
		if(e.includes("details.")){
		    if(i["details"] === undefined){
			i["details"] = {}
		    }
		    i["details"][e.split("details.")[1]] = i[e];
		    delete i[e]
		}
	    })
	    return i;
	});

	var res = this.api.applyTransaction({ add: this.rows });
	document.getElementById("rowCount").innerHTML = "(" + this.api.getDisplayedRowCount().toString() + ")"
    }
    get url(){
	return this.data.get("postUrl");
    }

    get deleteUrl(){
	return this.data.get("deleteUrl");
    }

    get editable(){
	return this.data.get("editable");
    }

    get redirectUrl(){
	return this.data.get("redirectUrl");
    }

    disconnect(){
	this.gridTarget.innerHTML = "";

    }

    selectAll(){
	this.api.selectAllFiltered()
    }


    deleteRows(e){
	e.preventDefault();
	var selectedNodes = this.api.getSelectedNodes();

	console.log(selectedNodes);
	if(selectedNodes.length == 0){
	    alert("Please select at least one row to delete");
	}else{
	    var selectedData = selectedNodes.map(node =>  {
		var data = {}
		data["id"] = node.data.id
		return data
	    });

	    Rails.ajax({
		url: this.deleteUrl,
		type: "DELETE",
		data: "payload=" +JSON.stringify(selectedData),
		error: (xhr,error) => {
		    alert("Data update failed. Please refresh");
		},
		success: (data) => {
		    window.location.reload();
		}
	    });
	}

	e.preventDefault();

    }

    clear() {
	if(confirm("Are you sure to clear all?")){
	    location.reload();
	}
    }

    back() {
	window.history.back();
    }



}
