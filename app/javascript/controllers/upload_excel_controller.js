import ApplicationController from './application_controller'
import "xlsx";

/* This is the custom StimulusReflex controller for Covid19Reflex.
* Learn more at: https://docs.stimulusreflex.com
*/
export default class extends ApplicationController {
    static targets = ["file"]
    connect() {
	console.log("Connected Excel upload");
	console.log(this.fileTarget);
    }
    convertMyanmarDobToEnglishDob(serial){
	var serial = serial+"";
	serial = serial.replace(/\s/g, '');
	var temp_num = "";
	for(var i=0; i<serial.length; i++) {
            switch(serial[i]){
            case '၀': temp_num += '0'; break;
            case 'ဝ': temp_num += '0'; break;
            case '၁': temp_num += '1'; break;
            case '၂': temp_num += '2'; break;
            case '၃': temp_num += '3'; break;
            case '၄': temp_num += '4'; break;
            case '၅': temp_num += '5'; break;
            case '၆': temp_num += '6'; break;
            case '၇': temp_num += '7'; break;
            case '၈': temp_num += '8'; break;
            case '၉': temp_num += '9'; break;
            case '0': temp_num += '0'; break;
            case '1': temp_num += '1'; break;
            case '2': temp_num += '2'; break;
            case '3': temp_num += '3'; break;
            case '4': temp_num += '4'; break;
            case '5': temp_num += '5'; break;
            case '6': temp_num += '6'; break;
            case '7': temp_num += '7'; break;
            case '8': temp_num += '8'; break;
            case '9': temp_num += '9'; break;
            default: temp_num += serial[i]; break;
            }
	}
	return temp_num;
    }

    getTableFromExcel(data) {
	var workbook = XLSX.read(data, {
            type: 'binary',
	    raw: true ,
	    cellText: true,
	    cellDates: true


	});

	var Sheet = workbook.SheetNames[0];
	console.log(Sheet);
	var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[Sheet]);
	//parse this data to inventory grid
	excelRows.map((e) => {
	    var item = e
	    console.log(item);

	    item.ssn_employee_no = item.remark;
	    delete item["remark"];
	    item.date_of_birth = this.convertMyanmarDobToEnglishDob(item.date_of_birth);
	    return item
	    //	    item[
	})
	console.log(excelRows);

	const event = new CustomEvent("updateGrid",{ detail: { rows: excelRows  } });

	window.dispatchEvent(event);
    }



    upload(){
	if(this.upload != true){
	    this.upload = true;

	    var fileUpload = this.fileTarget;

	    //Validate whether File is valid Excel filne.
	    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;

            if (typeof (FileReader) != "undefined") {
		var reader = new FileReader();

		//For Browsers other than IE.
		if (reader.readAsBinaryString) {

                    reader.onload = (e) => {
			this.getTableFromExcel(e.target.result);
                    };
                    reader.readAsBinaryString(fileUpload.files[0]);
		} else {
                    //For IE Browser.
                    reader.onload = (e) => {
			var data = "";
			var bytes = new Uint8Array(e.target.result);
			for (var i = 0; i < bytes.byteLength; i++) {
                            data += String.fromCharCode(bytes[i]);
			}

			this.getTableFromExcel(data);
                    };
                    reader.readAsArrayBuffer(fileUpload.files[0]);
		}
            } else {
		alert("This browser does not support HTML5.");
            }
	}
    }




}
