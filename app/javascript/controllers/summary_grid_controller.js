import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import ApplicationController from './application_controller'
import {Grid} from 'ag-grid-community';
import {LicenseManager} from "ag-grid-enterprise";


import "xlsx";
import Rails from "@rails/ujs";
import DateEditor from '../packs/date_cell_editor';
import Swal from 'sweetalert2';
import 'ag-grid-enterprise';
export default class extends ApplicationController {
    static targets = ["grid"];

    connect() {
    	this.rows = [];
	this.loadGrid();
    }

    loadGrid(){
	LicenseManager.setLicenseKey("CompanyName=Zwenexsys,LicensedGroup=C4M,LicenseType=MultipleApplications,LicensedConcurrentDeveloperCount=1,LicensedProductionInstancesCount=0,AssetReference=AG-010270,ExpiryDate=16_October_2021_[v2]_MTYzNDMzODgwMDAwMA==4bbfea46001dcd4e2023e71d63d7afc7");
	this.api = new Grid(this.gridTarget,this.gridOptions).gridOptions.api
	this.api.sizeColumnsToFit();

    }



    get gridOptions(){
	var rows = window.initialRows;
	console.log(rows);

	return {
	    columnDefs: columnDefs,
	    rowData: rows,



	    rowSelection: 'multiple',

	    defaultColDef: {
		resizable: true,
		sortable: true,
		filter: true // set filtering on for all columns
	    }
	}

    }




    disconnect(){
	this.gridTarget.innerHTML = "";

    }


    export() {
	this.api.exportDataAsExcel({});

    }

    exportForPayment(){
	var selectedNodes = this.api.getSelectedNodes();
	var selectedData = selectedNodes.map(node => node.data.id);
	console.log(selectedData.join(","));
	$("#ids").val(selectedData.join(","));
	$("#export").submit();
    }


}
