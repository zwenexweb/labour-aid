json.extract! township, :id, :ts_pcode, :name, :created_at, :updated_at
json.url township_url(township, format: :json)
