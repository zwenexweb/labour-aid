json.extract! extra_employee, :id, :ward, :street, :house_no, :name, :gender, :nric, :date_of_birth, :job, :workplace, :created_at, :updated_at
json.url extra_employee_url(extra_employee, format: :json)
