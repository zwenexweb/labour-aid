class ApplicationRequest < ApplicationRecord
  include Rails.application.routes.url_helpers
  include ActionView::Helpers::UrlHelper
  belongs_to :workplace
  has_many :employees, dependent: :destroy
  has_one_attached :attachment
  include AASM

  belongs_to :verify_by,class_name: "User", primary_key: "id", foreign_key: "verify_by",optional: true
  belongs_to :reject_by,class_name: "User", primary_key: "id", foreign_key: "reject_by",optional: true
  belongs_to :approve_by,class_name: "User", primary_key: "id", foreign_key: "approve_by", optional: true
  belongs_to :submit_by,class_name: "User", primary_key: "id", foreign_key: "submit_by", optional: true

#  validates :attachment,  blob: { content_type: :image } # supported options: :image, :audio, :video, :text
  aasm do
    state :draft, initial: true
    state :submitted
    state :rejected
    state :verified, :approved

    event :submit do
      transitions from: :draft, to: :submitted
    end

    event :verify do
      transitions from: :submitted, to: :verified
    end

    event :reject do
      transitions from: :submitted, to: :rejected
      transitions from: :verified, to: :rejected
      transitions from: :approved, to: :rejected
    end

    event :approve do
      transitions from: :verified, to: :approved
    end


  end

  def missing_columns
    my_missing_columns = []
    columns = [:organization_registration_no, :owner_phone,  :employer_ssn]
    columns.each do |item|

      if self.instance_eval(item.to_s).blank?
        my_missing_columns << item
      end
    end

    if !attachment.attached?
      my_missing_columns << :attachment
    end
    my_missing_columns
  end

  def self.view_columns
    {
      id: { source: "ApplicationRequest.id", cond: :eq},
      workplace: { source: "Workplace.name", cond: :like},
      state: { source: "ApplicationRequest.aasm_state", cond: :like},
      submitted_at: { source: "ApplicationRequest.created_at", cond: :like},
      verify_by: { source: "ApplicationRequest.id", cond: :like},
      approve_by: { source: "ApplicationRequest.id", cond: :like},
      last_action_at: { source: "Workplace.updated_at", cond: :like},
      actions: { source: "ApplicationRequest.id", cond: :eq }
    }
  end

  def custom_attributes
    actions = ""
    if aasm_state == "draft"
      actions += button_to(I18n.t("print"), print_application_request_path(),params: { id: self.to_sgid.to_s}, class: "btn btn-link float-left", method: :post, form:{ target: "_blank" })
      actions += link_to(I18n.t("edit"),edit_application_request_path(self),class: "btn btn-link float-left")
    end
    actions += link_to(I18n.t("view"),application_request_path(self),class: "btn btn-link float-left")
    {
      workplace: workplace.name ,
      state: aasm_state.humanize,
      verify_by: verify_by&.name,
      approve_by: approve_by&.name,
      submitted_at: created_at.strftime("%Y-%m-%d"),
      last_action_at: updated_at.strftime("%Y-%m-%d"),
       actions: actions.html_safe
    }
  end
end
