# frozen_string_literal: true
class Ability
  include CanCan::Ability

  def initialize(user)

    can :manage, :workplaces
    can :manage, :application_requests
    if user.admin? or user.sys_admin?
      can :manage, :users
      can :manage, :workplaces

      can :manage, Workplace
      if user.sys_admin?
        can :manage, :townships
      end
    end


    if user.accounting_manager?

      can :manage, Workplace, ts_pcode: user.townships.pluck(:ts_pcode)
      can :create, :workplaces
      can :manage, :application_requests
    end

    if user.company?
      can :manage, :workplaces
      can :manage, :application_requests
      can :manage, Workplace, owner_id: user.id


    end


    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
