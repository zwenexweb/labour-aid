# coding: utf-8
class Workplace < ApplicationRecord
  include Rails.application.routes.url_helpers
  include ActionView::Helpers::UrlHelper
  include AASM
  has_many :user_workplaces,dependent: :destroy
  has_many :users, through: :user_workplaces,dependent: :destroy
  belongs_to :township,:foreign_key => "ts_pcode", :primary_key => "ts_pcode"


  belongs_to :owner,class_name: "User", primary_key: "id", foreign_key: "owner_id",dependent: :destroy
  validates_presence_of :owner
  validates_presence_of :name
  validates_presence_of :address
  validates_presence_of :township




  has_many :application_requests, dependent: :destroy

  accepts_nested_attributes_for :owner,  reject_if: proc { |attributes| attributes['email'].blank? }

  aasm do
    state :pending, initial: true
    state :accepted
    state :rejected

    event :accept do
      transitions from: :pending, to: :accepted

    end

    event :reject do

      transitions from: :pending, to: :rejected
    end
  end


  def create_application_request
    if application_requests.where.not(aasm_state: "rejected").count != request_limit
      if accepted?
        ApplicationRequest.create(workplace: self,
                                  organization_name: self.name,
                                  address: self.address,
                                  owner_name: owner.name,
                                  ts_pcode: ts_pcode)
      end
    else
      application_requests.first
    end
  end

  def self.view_columns
    {
      id: { source: "Workplace.id",cond: :eq },
      name: { source: "Workplace.name",cond: :like },
      address: { source: "Workplace.address",cond: :like },
      owner: { source: "User.name", cond: :eq },
      owner_email: { source: "User.email", cond: :eq },
      township: { source: "Township.name", cond: :eq},
      actions: { source: "Workplace.id" }
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
    }
  end

  def custom_attributes
    actions = ""
    actions += link_to(I18n.t("view"),workplace_path(self),class: "btn btn-link float-left")
#    actions += link_to(I18n.t("edit"),edit_workplace_path(self),class: "btn btn-link float-left")
 #   actions += link_to(I18n.t("delete"),workplace_path(self),method: :delete, class: "btn btn-link float-left")



    { township: township&.name, owner: owner&.name, owner_email: owner&.email,actions: actions.html_safe}
  end

end
