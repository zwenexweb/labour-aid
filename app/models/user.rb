class User < ApplicationRecord
  include Rails.application.routes.url_helpers
  include ActionView::Helpers::UrlHelper

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,:masqueradable

  has_many :user_townships, dependent: :destroy
  has_many :townships, through: :user_townships


  has_many :workplaces, primary_key: "id", foreign_key: "owner_id", dependent: :destroy

  has_many :verfied_workplaces,class_name: "ApplicationRequest", primary_key: "id", foreign_key: "verify_by"
  has_many :rejected_workplaces,class_name: "ApplicationRequest", primary_key: "id", foreign_key: "reject_by"
  has_many :approved_workplaces,class_name: "ApplicationRequest", primary_key: "id", foreign_key: "approve_by"
  has_many :submitted_workplaces,class_name: "ApplicationRequest", primary_key: "id", foreign_key: "submit_by"

  def is_industry_user?
    if townships.present?
      townships.first.ts_pcode.include? "IND"
    else
      false
    end
  end

  def self.view_columns
    {
      name: { source: "User.name", cond: :like},
      organization_name: { source: "User.organization_name", cond: :like},
      email: { source: "User.email", cond: :like},
      user_role: { source: "User.user_role", cond: :like},
      title: { source: "User.title", cond: :like},
      township: { source: "User.ts_pcode", cond: :eq},
      actions: { source: "User.id", cond: :eq }

    }
  end

  def custom_attributes
    actions = ""
#    actions += link_to(I18n.t("reset_password"),"",class: "btn btn-link float-left")
    actions += link_to(I18n.t("view"),get_user_path(self),class: "btn btn-link float-left")
    actions += link_to(I18n.t("edit"),edit_user_path(self),class: "btn btn-link float-left")
    actions += link_to(I18n.t("delete"),destroy_user_path(self),method: :delete, class: "btn btn-link float-left")

    { township:  townships.pluck(:name).join(","), actions: actions.html_safe }
      #.merge({ actions: link_to("Login", Rails.application.routes.url_helpers.user_masquerade_path(self))})
  end

  def generate_application_requests
    if company?
      workplaces.map(&:create_application_request)
    end
  end




  def sys_admin?
    user_role == "SystemAdmin"
  end

  def admin?
    user_role == "Admin"
  end

  def company?
    user_role == "Company"
  end

  def accounting_manager?
    user_role == "AccountManager"
  end

  def account_manager?
    user_role == "AccountManager"
  end

  def verification_officer?
    user_role == "Verification Officer"
  end

  def approving_officer?
    user_role == "Approving Officer"
  end

  def readonly_admin?
    user_role == "Readonly Admin"
  end

  def printonly_admin?
    user_role == "Printonly Admin"
  end

  def my_managable_users
    if company?
      []
    elsif accounting_manager?
      User.where(id: my_workplaces.pluck(:owner_id))
    else
      User.all
    end
  end



  def my_managable_application_requests
    ApplicationRequest.joins(:workplace).merge(my_workplaces)
  end

  def my_workplaces
    if company?
      workplaces
    elsif accounting_manager?
      Workplace.where(ts_pcode: townships.pluck(:ts_pcode))
    elsif verification_officer?
      Workplace.where(ts_pcode: townships.pluck(:ts_pcode))
    elsif readonly_admin?
      Workplace.all
    else
      Workplace.all
    end
  end






  def my_creatable_roles
    case user_role
    when "SystemAdmin"
      User.roles
    when "Admin"
      User.roles
    when "AccountManager"
      User.roles.select{ |x| x == "Company" }

    when "Verification Officer"
      User.roles.select{ |x| x == "Verification Officer" }
    when "Approving Officer"
      User.roles.select{ |x| x == "Approving Officer" }
    when "Company"
      []
    end
  end

  def self.roles
    ["Admin", "SystemAdmin","AccountManager", "Verification Officer", "Approving Officer", "Company", "Readonly Admin", "Printonly Admin"]
  end
end
