class UserTownship < ApplicationRecord
  belongs_to :user
  belongs_to :township,:foreign_key => "ts_pcode", :primary_key => "ts_pcode"
  validates_uniqueness_of :user, scope: [:township]
end
