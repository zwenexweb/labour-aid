class CompaniesBase < ActiveRecord::Base
  self.abstract_class = true

  connects_to database: { writing: :companies, reading: :companies }

  def self.view_columns
    company = {}
    new.attributes.keys.each do |x|
      name = "#{self.name.to_s}.#{x}"
      company["#{x}".to_sym] = { source: name, cond: :like }
    end
    company
  end

  def self.map_tables x
    case x.to_s
    when "dol_companies"
      CompaniesDol
    when "sme_companies"
      CompaniesSme
    when "industry_companies"
      CompaniesIndustry
    when "mic_companies"
      CompaniesMic
    end
  end
end
