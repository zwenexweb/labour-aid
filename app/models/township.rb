class Township < ApplicationRecord
  def self.view_columns
    {
      ts_pcode: { source: "Township.ts_pcode", cond: :like},
      name: { source: "Township.name", cond: :like}
    }
  end
end
