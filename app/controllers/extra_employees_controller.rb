class ExtraEmployeesController < ApplicationController
  before_action :set_extra_employee, only: [:show, :edit, :update, :destroy]

  # GET /extra_employees
  # GET /extra_employees.json
  def index
    @extra_employees = ExtraEmployee.all
  end

  # GET /extra_employees/1
  # GET /extra_employees/1.json
  def show
  end

  # GET /extra_employees/new
  def new
    @extra_employee = ExtraEmployee.new
  end

  # GET /extra_employees/1/edit
  def edit
  end

  # POST /extra_employees
  # POST /extra_employees.json
  def create
    @extra_employee = ExtraEmployee.new(extra_employee_params)

    respond_to do |format|
      if @extra_employee.save
        format.html { redirect_to @extra_employee, notice: 'Extra employee was successfully created.' }
        format.json { render :show, status: :created, location: @extra_employee }
      else
        format.html { render :new }
        format.json { render json: @extra_employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /extra_employees/1
  # PATCH/PUT /extra_employees/1.json
  def update
    respond_to do |format|
      if @extra_employee.update(extra_employee_params)
        format.html { redirect_to @extra_employee, notice: 'Extra employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @extra_employee }
      else
        format.html { render :edit }
        format.json { render json: @extra_employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /extra_employees/1
  # DELETE /extra_employees/1.json
  def destroy
    @extra_employee.destroy
    respond_to do |format|
      format.html { redirect_to extra_employees_url, notice: 'Extra employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_extra_employee
      @extra_employee = ExtraEmployee.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def extra_employee_params
      params.require(:extra_employee).permit(:ward, :street, :house_no, :name, :gender, :nric, :date_of_birth, :job, :workplace)
    end
end
