class SummaryController < ApplicationController
  before_action :authenticate_user!
  skip_before_action :authenticate_user!, only: [:print]
  layout "print", :only => [ :print ]

  def index
    application_requests =  ApplicationRequest.all.merge(current_user.my_workplaces).joins(:workplace).includes(:workplace).where.not(aasm_state: "draft")

    application_requests = application_requests.merge(current_user.my_workplaces)
    @start_date =  Date.today
    @end_date = Date.today
    if params[:start_time].present?
      @start_date = Date.parse(params[:start_time])
    end
    if params[:end_time].present?
      @end_date = Date.parse(params[:end_time])
    end


    if params[:filter_type] == "verified"
      application_requests = application_requests
                               .where(aasm_state: "verified")
      application_requests = application_requests
                               .where(verify_at: @start_date.beginning_of_day..@end_date.end_of_day)
    elsif params[:filter_type] == "approved"
      application_requests = application_requests
                               .where(aasm_state: "approved")
      application_requests = application_requests
                               .where(approve_at: @start_date.beginning_of_day..@end_date.end_of_day)
    elsif params[:filter_type] == "rejected"
      application_requests = application_requests
                               .where(aasm_state: "rejected")
      application_requests = application_requests
                               .where(reject_at: @start_date.beginning_of_day..@end_date.end_of_day)

    else
      application_requests = application_requests
                               .where(submit_at: @start_date.beginning_of_day..@end_date.end_of_day)
                               .or(application_requests.where(created_at: @start_date.beginning_of_day..@end_date.end_of_day))
    end


    @summary_json = application_requests.map{ |x| SummarySerializer.new(x,user_id: current_user.id).to_h }.to_json
  end

  def print
    if current_user.present?
      if current_user.admin? or current_user.sys_admin? or current_user.printonly_admin?
        @application_requests = ApplicationRequest.where(id: params[:ids].split(","))
      else
        head 404
      end
    else
      if params[:token] == "czukmzaq5dprgy6draxxfsfy4ja347hvcponuqne94ywnbhcwez3vrq62wiqxtm7t4gft5y6wj5zqz2uttfdfxd2"
        @application_requests = ApplicationRequest.where(id: params[:ids].split(","))
      else
        head 404
      end
    end
  end
end
