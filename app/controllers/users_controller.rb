class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update, :destroy, :update]

  def index
    respond_to do |format|
      format.html
      format.json { render json: UserDatatable.new(params, user: current_user) }
    end
    authorize! :manage, :users
  end


  def show
    authorize! :manage, :users
    unless current_user.my_managable_users.include? @user
      redirect_to root_path and return
    end
  end

  def new
    @user = User.new

  end

  def edit
  end


  def create

    @user = User.new(user_params)
    @user.townships  = params[:user][:townships].reject{ |x| x.blank? }.map{ |x| Township.find_by_ts_pcode(x) }
    respond_to do |format|
      if @user.save
        format.html { redirect_to  get_user_path(@user), notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  def update

    authorize! :manage, :users
    unless current_user.my_managable_users.include? @user
      redirect_to root_path and return
    end
    if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end

    respond_to do |format|
      @user.townships  = params[:user][:townships].reject{ |x| x.blank? }.map{ |x| Township.find_by_ts_pcode(x) }
      @user.save
      if @user.update(user_params)
        format.html { redirect_to get_user_path(@user), notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy

    authorize! :manage, :users
    unless current_user.my_managable_users.include? @user
      redirect_to root_path and return
    end
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def user_params
      params.require(:user).permit(:email,:ts_pcode, :password, :password_confirmation, :name, :title, :organization_name, :user_role, :work_place_id, :created_by)
    end
end
