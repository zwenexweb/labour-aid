class PublicWorkplacesController < ApplicationController

  def index
    if current_user.present?
      redirect_to workplaces_path(aasm_state: "pending")
    end
  end

  def new
    @workplace = Workplace.new
    @workplace.owner = User.new
  end

  def create

    @workplace = Workplace.new(workplace_params)
    my_password = SecureRandom.uuid
    @workplace.owner.password = my_password
    @workplace.owner.password_confirmation = my_password
    @workplace.owner.user_role = "Company"
    respond_to do |format|
      if verify_recaptcha(model: @workplace) && @workplace.save
        format.html { redirect_to success_form_path, notice: 'Workplace was successfully created.' }

        format.json { render :show, status: :created, location: @workplace }
      else
        format.html { render :new }
        format.json { render json: @workplace.errors, status: :unprocessable_entity }
      end
    end
  end

  def success
  end

  private

  # Only allow a list of trusted parameters through.
  def workplace_params
    params.require(:workplace).permit(:name, :address,:ts_pcode, :request_limit, :created_by,owner_attributes: [:name,:email,:password,:password_confirmation, :organization_name, :title,:user_role,:phone_number,:zone])
  end
end
