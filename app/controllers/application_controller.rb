class ApplicationController < ActionController::Base

  before_action :masquerade_user!


  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :not_found, content_type: 'text/html' }
      format.html { redirect_to root_path }
      format.js   { head :not_found, content_type: 'text/html' }
    end
  end
end
