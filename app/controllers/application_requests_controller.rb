class ApplicationRequestsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_application_request, only: [:show, :edit, :update, :destroy, :new_employees, :batch_create, :employee_list, :batch_delete, :change_state]
  skip_before_action :verify_authenticity_token
  layout "application_request_print", :only => [ :print ]

  # GET /application_requests
  # GET /application_requests.json
  def index
    current_user.generate_application_requests

    respond_to do |format|
      format.html
      format.json { render json: ApplicationRequestDatatable.new(params, user: current_user) }
    end
  end

  def change_state
    if current_user.readonly_admin?
      redirect_to root_path
    end
    if params[:state].to_s == "submit"
      if @application_request.employees.count == 0
        redirect_to @application_request, notice: 'Please fill the employees' and return

      end
      if @application_request.missing_columns.count > 0
        redirect_to @application_request, notice: 'Please fill the remaining fields' and return
      end

      if !@application_request.attachment.attached?
        redirect_to @application_request, notice: 'Please attach the attachment' and return
      end
    end


    @application_request.update("#{params[:state].to_s}_at": Date.today)
    @application_request.update("#{params[:state].to_s}_by": current_user)
    @application_request.send("#{params[:state].to_s}!")
    redirect_to @application_request
  end

  def employee_list
    render partial: "employee_list", locals: { employees_json: @employees_json }
  end

  def new_employees
    unless current_user.company?
      redirect_to root_path
    end
  end

  def batch_delete
    payload = JSON.parse(params["payload"])
    payload.each do |item|
      employee= Employee.find_by_id(item["id"])
      if current_user.my_managable_application_requests.include? employee.application_request
        update_data = employee.destroy
      end

    end
  end

  def batch_create
    unless current_user.company?
      redirect_to root_path
    end
    if current_user.readonly_admin?
      redirect_to root_path
    end
    rows = params["rows"]
    items = rows.each do |item|
      item = Employee.new(item.permit!.except("school", "inventory_item_category", "id"))
      item.application_request = @application_request
      item.save
    end
    redirect_to @application_request
  end

  # GET /application_requests/1
  # GET /application_requests/1.json
  def show
    if current_user.my_managable_application_requests.include? @application_request
      @employees_json  = (@application_request.employees.map do |item| EmployeeSerializer.new(item, params: params).to_hash end).to_json
      puts @employees_json
    else
      redirect_to root_path
    end
  end


  def data_update

    payload = JSON.parse(params["payload"])
    employee= Employee.find_by_id(params["id"])
    if current_user.my_managable_application_requests.include? employee.application_request
      update_data = employee.update(payload)
    end
    employee.reload
    render json: employee
  end

  # GET /application_requests/new
  def new
    @application_request = ApplicationRequest.new
  end

  # GET /application_requests/1/edit
  def edit
    unless current_user.my_managable_application_requests.include? @application_request
      if current_user.readonly_admin?
        redirect_to root_path
      end
    end
  end

  def print
  end

  # POST /application_requests
  # POST /application_requests.json
  def create
    @application_request = ApplicationRequest.new(application_request_params)
    if @application_request.status == "VERIFIED"
      @application_request.verified_at = DateTime.now
      @application_request.rejected_at = nil
      @application_request.approved_at = nil
      @application_request.verified_by = current_user.id
      @application_request.approved_by = nil
    elsif @application_request.status == "REJECTED"
      @application_request.rejected_at = DateTime.now
      @application_request.verified_at = nil
      @application_request.approved_at = nil
      @application_request.verified_by = nil
      @application_request.approved_by = nil
    elsif @application_request.status == "APPROVED"
      @application_request.approved_at = DateTime.now
      @application_request.verified_at = nil
      @application_request.rejected_at = nil
      @application_request.approved_by = current_user.id
      @application_request. verified_by = nil
    else
      @application_request.approved_at = nil
      @application_request.verified_at = nil
      @application_request.rejected_at = nil
      @application_request.verified_by = nil
      @application_request.approved_by = nil
    end
    respond_to do |format|
      if @application_request.save
        format.html { redirect_to @application_request, notice: 'Application request was successfully created.' }
        format.json { render :show, status: :created, location: @application_request }
      else
        format.html { render :new }
        format.json { render json: @application_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /application_requests/1
  # PATCH/PUT /application_requests/1.json
  def update
    respond_to do |format|
      if @application_request.update(application_request_params)
        format.html { redirect_to @application_request, notice: 'Application request was successfully updated.' }
        format.json { render :show, status: :ok, location: @application_request }
      else
        format.html { render :edit }
        format.json { render json: @application_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /application_requests/1
  # DELETE /application_requests/1.json
  def destroy
    @application_request.destroy
    respond_to do |format|
      format.html { redirect_to application_requests_url, notice: 'Application request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def print
    id = GlobalID::Locator.locate_signed(params[:id]).id
    @application_requests = ApplicationRequest.where(id: id)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_application_request
      @application_request = ApplicationRequest.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def application_request_params
      params.require(:application_request).permit(:work_place_id, :status, :verified_at, :rejected_at, :approved_at, :verified_by, :approved_by, :employee_list, :organization_name, :organization_registration_no, :ts_pcode, :phone, :employer_ssn, :is_running,:address, :owner_name, :owner_phone,:attachment, :remark)
    end
end
