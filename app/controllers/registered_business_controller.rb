class RegisteredBusinessController < ApplicationController
  def index

    @companies = [:dol_companies,
                  :mic_companies,
                  :sme_companies,
                  :industry_companies]
    @type = params[:type] || "dol_companies"
    respond_to do |format|
      format.html
      format.json {
        case params[:type].to_s
        when "dol_companies"
          render json: CompaniesDolDatatable.new(params, user: current_user)
        when "mic_companies"
          render json: CompaniesMicDatatable.new(params, user: current_user)
        when "sme_companies"
          render json: CompaniesSmeDatatable.new(params, user: current_user)
        when "industry_companies"
          render json: CompaniesIndustryDatatable.new(params, user: current_user)
        else
          render json: CompaniesIndustryDatatable.new(params, user: current_user)
        end
      }
    end
  end
end
