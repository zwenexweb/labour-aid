class WorkplacesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_workplace, only: [:show, :edit, :update, :destroy, :change_state]

  # GET /workplaces
  # GET /workplaces.json
  def index

    respond_to do |format|
      format.html
      format.json { render json: WorkplaceDatatable.new(params, user: current_user, aasm_state: params[:aasm_state]) }
    end
    authorize! :manage, :workplaces
  end

  # GET /workplaces/1
  # GET /workplaces/1.json
  def show
    @owner = @workplace.owner
    authorize! :read, @workplace
  end

  # GET /workplaces/new
  def new
    @workplace = Workplace.new
    @workplace.owner = User.new
  end

  def change_state
    authorize! :read, @workplace
    unless params[:state] == "reset"
      @workplace.send("#{params[:state].to_s}!")
    else
      @workplace.owner.send_reset_password_instructions
    end
    if params[:state] == "accept"
      @workplace.owner.send_reset_password_instructions
    end
    redirect_to @workplace
  end



  # GET /workplaces/1/edit
  def edit


  end

  # POST /workplaces
  # POST /workplaces.json
  def create

    @workplace = Workplace.new(workplace_params)

    @workplace.created_by = current_user.id
    respond_to do |format|
      if @workplace.save
        @workplace.accept!
        format.html { redirect_to @workplace, notice: 'Workplace was successfully created.' }

        format.json { render :show, status: :created, location: @workplace }
      else
        format.html { render :new }
        format.json { render json: @workplace.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /workplaces/1
  # PATCH/PUT /workplaces/1.json
  def update
    if params[:owner_attributes][:password].blank? && params[:owner_attributes][:password_confirmation].blank?
      params[:owner_attributes].delete(:password)
      params[:owner_attributes].delete(:password_confirmation)
    end
    authorize! :update, @workplace
    respond_to do |format|
      if @workplace.update(workplace_params)
        @workplace.update(created_by: current_user.id)
        format.html { redirect_to @workplace, notice: 'Workplace was successfully updated.' }
        format.json { render :show, status: :ok, location: @workplace }
      else
        format.html { render :edit }
        format.json { render json: @workplace.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /workplaces/1
  # DELETE /workplaces/1.json
  def destroy
    authorize! :destroy, @workplace
    @workplace.destroy
    respond_to do |format|
      format.html { redirect_to workplaces_url, notice: 'Workplace was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_workplace
      @workplace = Workplace.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def workplace_params
      params.require(:workplace).permit(:name, :address,:ts_pcode, :request_limit, :created_by,owner_attributes: [:name,:email,:password,:password_confirmation, :organization_name, :title,:user_role])
    end
end
