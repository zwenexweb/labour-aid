class AddIndexToTownships < ActiveRecord::Migration[6.0]
  def change
    add_index :townships, :ts_pcode, unique: true
  end
end
