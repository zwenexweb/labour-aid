class CreateEmployees < ActiveRecord::Migration[6.0]
  def change
    create_table :employees do |t|
      t.string :name
      t.string :gender
      t.date :date_of_birth
      t.string :nric
      t.string :position
      t.float :salary
      t.string :ssn_employee_no
      t.belongs_to :application_request, null: false, foreign_key: true

      t.timestamps
    end
  end
end
