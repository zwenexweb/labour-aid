class CreateExtraEmployees < ActiveRecord::Migration[6.0]
  def change
    create_table :extra_employees do |t|
      t.string :ward
      t.string :street
      t.string :house_no
      t.string :name
      t.string :gender
      t.string :nric
      t.date :date_of_birth
      t.string :job
      t.string :workplace

      t.timestamps
    end
  end
end
