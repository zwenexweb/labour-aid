class AddNewFieldsToApplicationRequests < ActiveRecord::Migration[6.0]
  def change
    add_column :application_requests, :organization_name, :string
    add_column :application_requests, :organization_registration_no, :string
    add_column :application_requests, :owner_name, :string
    add_column :application_requests, :owner_phone, :string
    add_column :application_requests, :is_running, :boolean
    add_column :application_requests, :address, :string
    add_column :application_requests, :ts_pcode, :string
    add_column :application_requests, :employer_ssn, :string
  end
end
