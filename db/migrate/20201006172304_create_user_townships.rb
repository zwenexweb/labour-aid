class CreateUserTownships < ActiveRecord::Migration[6.0]
  def change
    create_table :user_townships do |t|
      t.belongs_to :user, null: false, foreign_key: true
      t.string :ts_pcode

      t.timestamps
    end
  end
end
