class AddDatesToApplicationRequests < ActiveRecord::Migration[6.0]
  def change
    add_column :application_requests, :verify_at, :date
    add_column :application_requests, :submit_at, :date
    add_column :application_requests, :reject_at, :date
    add_column :application_requests, :approve_at, :date
  end
end
