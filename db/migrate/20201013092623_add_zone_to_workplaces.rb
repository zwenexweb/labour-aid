class AddZoneToWorkplaces < ActiveRecord::Migration[6.0]
  def change
    add_column :workplaces, :zone, :string
  end
end
