class CreateApplicationRequests < ActiveRecord::Migration[6.0]
  def change
    create_table :application_requests do |t|
      t.integer :work_place_id
      t.string :status
      t.datetime :verified_at
      t.datetime :rejected_at
      t.datetime :approved_at
      t.integer :verified_by
      t.integer :approved_by
      t.jsonb :employee_list

      t.timestamps
    end
  end
end
