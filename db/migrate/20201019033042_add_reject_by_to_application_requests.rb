class AddRejectByToApplicationRequests < ActiveRecord::Migration[6.0]
  def change
    add_column :application_requests, :reject_by, :integer
  end
end
