class CreateWorkplaces < ActiveRecord::Migration[6.0]
  def change
    create_table :workplaces do |t|
      t.string :name, unique: true  
      t.text :address
      t.integer :request_limit, default: 1
      t.integer :created_by

      t.timestamps
    end
  end
end
