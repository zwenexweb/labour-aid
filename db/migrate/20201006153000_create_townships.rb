class CreateTownships < ActiveRecord::Migration[6.0]
  def change
    create_table :townships do |t|
      t.string :ts_pcode
      t.string :name

      t.timestamps
    end
  end
end
