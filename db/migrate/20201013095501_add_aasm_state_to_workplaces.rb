class AddAasmStateToWorkplaces < ActiveRecord::Migration[6.0]
  def change
    add_column :workplaces, :aasm_state, :string
  end
end
