class AddWorkPlaceIdToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :work_place_id, :integer
  end
end
