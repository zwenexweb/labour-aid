class AddAasmStateToApplicationRequests < ActiveRecord::Migration[6.0]
  def change
    add_column :application_requests, :aasm_state, :string
  end
end
