class BelongsToWorkplaceToApplicationRequests < ActiveRecord::Migration[6.0]
  def change
    change_table :application_requests do |t|
      t.belongs_to :workplace
    end
  end
end
