class AddTsPcodeToWorkplaces < ActiveRecord::Migration[6.0]
  def change
    add_column :workplaces, :ts_pcode, :string
  end
end
