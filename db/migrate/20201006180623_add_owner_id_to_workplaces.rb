class AddOwnerIdToWorkplaces < ActiveRecord::Migration[6.0]
  def change
    change_table :workplaces do |t|
      t.integer :owner_id
    end
  end
end
