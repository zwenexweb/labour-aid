class RemoveColumnsOfApplicationRequest < ActiveRecord::Migration[6.0]
  def change
    remove_column :application_requests, :verified_at
    remove_column :application_requests, :rejected_at
    remove_column :application_requests, :approved_at

    remove_column :application_requests, :verified_by

    remove_column :application_requests, :approved_by
  end
end
