class AddCreatedByToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :created_by, :integer
  end
end
