class AddPersonToApplicationRequests < ActiveRecord::Migration[6.0]
  def change
    add_column :application_requests, :verify_by, :integer
    add_column :application_requests, :approve_by, :integer
    add_column :application_requests, :submit_by, :integer
  end
end
