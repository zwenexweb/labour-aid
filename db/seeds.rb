# Dir[Rails.root.join('db', 'seeds', '*.rb')].sort.each do |seed|
#   load seed
# end
# Dir[Rails.root.join('db', 'seeds', Rails.env, '*.rb')].sort.each do |seed|
#   load seed
# end

User.create( email: "admin@yangon.gov.mm", password: '12345678', password_confirmation: '12345678', user_role: "Admin")
User.create( email: "user@zwenex.com", password: '12345678', password_confirmation: '12345678', user_role: "SystemAdmin")
User.create( email: "approve@yangon.gov.mm", password: '12345678', password_confirmation: '12345678', user_role: "Approving Officer")
User.create( email: "verify@yangon.gov.mm", password: '12345678', password_confirmation: '12345678', user_role: "Verification Officer")
User.create( email: "manage@yangon.gov.mm", password: '12345678', password_confirmation: '12345678', user_role: "AccountManager")
User.create( email: "company@yangon.gov.mm", password: '12345678', password_confirmation: '12345678', user_role: "Company")
