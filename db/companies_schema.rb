# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_10_19_033841) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "application_requests", force: :cascade do |t|
    t.integer "work_place_id"
    t.string "status"
    t.jsonb "employee_list"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "aasm_state"
    t.bigint "workplace_id"
    t.string "organization_name"
    t.string "organization_registration_no"
    t.string "owner_name"
    t.string "owner_phone"
    t.boolean "is_running"
    t.string "address"
    t.string "ts_pcode"
    t.string "employer_ssn"
    t.date "verify_at"
    t.date "submit_at"
    t.date "reject_at"
    t.date "approve_at"
    t.integer "verify_by"
    t.integer "approve_by"
    t.integer "submit_by"
    t.integer "reject_by"
    t.index ["workplace_id"], name: "index_application_requests_on_workplace_id"
  end

  create_table "companies_dol", id: false, force: :cascade do |t|
    t.string "state_region", limit: 255
    t.string "township", limit: 255
    t.string "urban_rural", limit: 255
    t.string "ownership", limit: 255
    t.string "business_name", limit: 255
    t.string "address_and_contact", limit: 255
    t.string "produce", limit: 255
    t.string "isic_code", limit: 255
    t.string "ssb", limit: 255
    t.string "ec_contract", limit: 255
    t.string "citizen_male_salary", limit: 255
    t.string "citizen_female_salary", limit: 255
    t.string "citizen_male_daily", limit: 255
    t.string "citizen_female_daily", limit: 255
    t.string "foreigner_male", limit: 255
    t.string "foreigner_female", limit: 255
    t.string "total_male", limit: 255
    t.string "total_female", limit: 255
    t.string "total", limit: 255
    t.string "tspcode", limit: 255
  end

  create_table "companies_industry", id: false, force: :cascade do |t|
    t.string "business_owner", limit: 255
    t.string "business_type_and_address", limit: 800
    t.string "registration_no", limit: 255
    t.string "registered_at", limit: 255
    t.string "phone_no", limit: 255
    t.string "township", limit: 255
    t.string "tspcode", limit: 255
  end

  create_table "companies_mic", id: false, force: :cascade do |t|
    t.serial "no", null: false
    t.string "company_name", limit: 255
    t.string "company_category", limit: 255
    t.string "phone_no", limit: 255
    t.string "email", limit: 255
    t.string "website", limit: 255
    t.string "address", limit: 255
    t.string "business_sector", limit: 255
    t.string "permit_or_endorsement", limit: 255
    t.string "local_or_foreign_investment", limit: 255
    t.string "states_region", limit: 255
    t.string "township", limit: 255
    t.string "tspcode", limit: 255
  end

  create_table "companies_myco", id: false, force: :cascade do |t|
    t.string "state_region", limit: 255
    t.string "address", limit: 255
    t.string "company_registration_number", limit: 255
    t.string "registration_date", limit: 255
    t.string "company_name_eng", limit: 255
    t.string "company_name_mm", limit: 255
    t.string "company_type", limit: 255
    t.string "company_state", limit: 255
    t.string "tspcode", limit: 255
  end

  create_table "companies_sme", id: false, force: :cascade do |t|
    t.string "sme_member_card_id", limit: 255
    t.string "entrepreneur_name", limit: 255
    t.string "business_name", limit: 255
    t.string "address_mm", limit: 255
    t.string "business_sector", limit: 255
    t.string "business_operations", limit: 255
    t.string "workers", limit: 255
    t.string "phone_no", limit: 255
    t.string "note", limit: 255
    t.string "business_size", limit: 255
    t.string "status", limit: 255
    t.string "application_status", limit: 255
    t.string "tspcode", limit: 255
  end

  create_table "companies_yric", id: false, force: :cascade do |t|
    t.string "company_name", limit: 255
    t.string "authorization_approval_order_no", limit: 255
    t.string "approval_date", limit: 255
    t.string "business_operation_type", limit: 552
    t.string "company_address", limit: 255
    t.string "contact_name", limit: 255
    t.string "contact_ph_no", limit: 255
    t.string "contact_address", limit: 255
    t.string "contanct_email", limit: 255
    t.string "jv_or_others", limit: 255
    t.string "terminate_or_not_aaccords_to_107_20", limit: 255
    t.string "tspcode", limit: 255
  end

  create_table "employees", force: :cascade do |t|
    t.string "name"
    t.string "gender"
    t.date "date_of_birth"
    t.string "nric"
    t.string "position"
    t.float "salary"
    t.string "ssn_employee_no"
    t.bigint "application_request_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_request_id"], name: "index_employees_on_application_request_id"
  end

  create_table "extra_employees", force: :cascade do |t|
    t.string "ward"
    t.string "street"
    t.string "house_no"
    t.string "name"
    t.string "gender"
    t.string "nric"
    t.date "date_of_birth"
    t.string "job"
    t.string "workplace"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "townships", force: :cascade do |t|
    t.string "ts_pcode"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ts_pcode"], name: "index_townships_on_ts_pcode", unique: true
  end

  create_table "user_townships", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "ts_pcode"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_user_townships_on_user_id"
  end

  create_table "user_workplaces", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "workplace_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_user_workplaces_on_user_id"
    t.index ["workplace_id"], name: "index_user_workplaces_on_workplace_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "name"
    t.string "title"
    t.string "organization_name"
    t.string "user_role"
    t.integer "work_place_id"
    t.integer "created_by"
    t.string "ts_pcode"
    t.string "phone_number"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "workplaces", force: :cascade do |t|
    t.string "name"
    t.text "address"
    t.integer "request_limit", default: 1
    t.integer "created_by"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "ts_pcode"
    t.integer "owner_id"
    t.string "zone"
    t.string "aasm_state"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "employees", "application_requests"
  add_foreign_key "user_townships", "users"
  add_foreign_key "user_workplaces", "users"
  add_foreign_key "user_workplaces", "workplaces"
end
